import discordAkairo from 'discord-akairo';

export default class PickCommand extends discordAkairo.Command {
	constructor() {
		super('pick', {
			aliases: ['pick'],
			description: {
				content: "Use this to decide what to do with your life!"
			},
			category: 'Fun',
			args: [
				{
					id: 'items',
					match: 'none',
					prompt: {
						start: [
							'What items would you like to pick from?',
							'Type them in separate messages.',
							'Type `stop` when you are done.'
						],
						infinite: true
					}
				}
			]
		});
	}

	exec(message, { items }) {
		const picked = items[Math.floor(Math.random() * items.length)];

		if (message.channel.sendable)
			message.util.reply(`I pick ${picked.trim()}!`);
		else {
			let addon = {}
			if (message.guild)
				addon = this.client.util.embed().setFooter(`This command was executed on ${message.guild.name}`);

			message.author.send(`I pick ${picked.trim()}!`, addon).catch();
		}
	}
}