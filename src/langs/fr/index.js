export default {
  'No description available.': "Ill n'y a pas des description disponible",
  "Total Commands: {0}": 'Total des commandes: {0}',
  "The language has been changed to {0} **{1}**.": "La langue a changé à {0} **{1}**.",

  'Plays the audio of a Youtube video.': 'Jouer la musique de YouTube',
  "Welcome channel": "La chanelle des bienvenue",
  "You need to have the configuration key `mutedrole` set in order for this command to work.": "Tu doi metre `mutedrole` dans ton configuration deve essere impostata.",
}